<?php include"model/config.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>todolist</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

</head>
<body class="">
    <form action="model/insert.php" method="POST">
<div class="container">
    <div class=" row justifay-content-center fo m-auto   bg-white mt-3 py-3 col-md-6" >
        <h3 class="text-center text-primary font-monospace">todo list</h3>
        <div class="col-8">
            <input type="text" name=" list" class="form-control" autofocus>
        </div>
        <div class="col-2">
            <button class="btn btn-outline-primary">ADD</button>
        </div>
    </div>
</div>
</form> 

<?php
    $rawDATA = mysqli_query($con , "SELECT * FROM `todo`");
?>

<div class="container">
<div class=" col-8 bg-white m-auto mt-3">
<table class="table ">
    <tbody>
        <?php
            $rawDATA = mysqli_query($con , "SELECT * FROM `todo`");?>
        <?php
        while($row = mysqli_fetch_array($rawDATA)){
            $background = ($row["done"] == 1) ? "success" : "warning";
            $label = ($row["done"] == 1) ? "undo" : "done";

        ?>
        <tr class="table-<?= $background?>">
            <td><?php echo$row['title']?></td>
            <td style="width: 10%;"><a href="model/delete.php?id=<?php echo$row['id'] ?>" class="btn btn-danger">delet</a></td>
            <td style="width: 10%;"><a href="model/toggle.php?id=<?php echo$row['id'] ?>" class="btn btn-primary" ><?= $label?></a></td>
        </tr>
        <?php
}
    ?>
    </tbody>
</table>
</div> 
</div> 
</body>
</html>